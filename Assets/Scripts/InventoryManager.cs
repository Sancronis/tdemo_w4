﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public PlayerController playerController;
    public EquipmentObject[] playerInventory;

    public Image hotBar1;
    public Image hotBar2;
    public Image hotBar3;
    public Image hotBar4;

    public EquipmentObject oldItem; //Used to store what used to be in the pickup item.

    public AudioSource itemPickUp;


    // Start is called before the first frame update
    public void updateIcons()
    {
        switch (playerController.currentHotkey)
        {
            case 1:
                if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar1.sprite = null; }
                else { hotBar1.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                break;
            case 2:
                if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar2.sprite = null; }
                else { hotBar2.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                break;
            case 3:
                if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar3.sprite = null; }
                else { hotBar3.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                break;
            case 4:
                if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar4.sprite = null; }
                else { hotBar4.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
    if (Input.GetKeyDown(KeyCode.E) && playerController.equipmentPickUp) 
        {
//            if (playerController.currentHotkey == 1) { playerInventory[0] }
            oldItem = playerController.currentPickUp.storedItem;
            playerController.currentPickUp.storedItem = playerInventory[playerController.currentHotkey - 1];
            playerInventory[playerController.currentHotkey-1] = oldItem;
            itemPickUp.Play();
            updateIcons();
/*            switch (playerController.currentHotkey)
            {
                case 1:
                    if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar1.sprite = null; }
                    else { hotBar1.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                    break;
                case 2:
                    if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar2.sprite = null; }
                    else { hotBar2.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                    break;
                case 3:
                    if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar3.sprite = null; }
                    else { hotBar3.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                    break;
                case 4:
                    if (playerInventory[playerController.currentHotkey - 1] == null) { hotBar4.sprite = null; }
                    else { hotBar4.sprite = playerInventory[playerController.currentHotkey - 1].equipmentIcon; }
                    break;
            }            */
        }
    }
}
