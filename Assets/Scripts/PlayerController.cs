using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public int health = 1000; //Variables here could be changed.
    public int morale = 1000;
    public int corruption = 0;
    public int corruptionRate = 5;
    public int moraleRate = 20;

//    public float tickTime = 1.0f;
//    public float lastTick = 0.0f;

    public bool alive = true;
    public bool atShrine = false;
    public bool inCorruptionZone = false;
    public bool equipmentPickUp = false;

    public Shrine currentShrine;
    public PickUpItem currentPickUp;

    public int currentHotkey = 1;
    public SelectedItemMover selectedItemMover;

    public int berserkerTimeLeft = 0;
    private Animator anim;
    public void ProcessMorale() //No check for time as this is processed regularly by Update()
    {
    if (morale>990)
        {
            corruption -= corruptionRate * 20;
		}
    else if (morale>900)
        {
            corruption -= corruptionRate * 5;
		}
    else if (morale>750)
        {
            corruption -= corruptionRate * 2;
        }
    else if (morale>500)
        {
            corruption -= corruptionRate;
		}
    if (corruption<0)
        {
            corruption = 0;
		}
    if (morale>500)
        {
            morale -= moraleRate;
		}
	}


    void ProcessDeath()
    {
        //Swap character state to dead. This should prevent movement, being attacked, or AI targetting the player.
        alive = false;
        anim.SetTrigger("Death");
        //Swap back to alive after 5 seconds.
        StartCoroutine("Resurrect");        
    }

    IEnumerator Resurrect()
    {
        anim.SetTrigger("Ressurection");
        yield return new WaitForSeconds(5);
        health = 1000;
        ModifyCorruption(200);
        ModifyMorale(-300);
        alive = true;
    }

    void processCorruptionDefeat()
    {
        corruption = 0; //End the game here. You've become too evil. This should probably give a defeat screen with a menu to return to the start.
        UnityEngine.Debug.Log("Game would be lost here.");
        GameObject.Find("GameOverScreen").GetComponent<GameOverSceneController>().TriggerGameOverScene();
    }

    public void ModifyHealth(int healthChange) //Pass positive for healing or negative for damage.
    {
        if (alive == true)
        {
            health = health + healthChange;
            if (health > 1000) { health = 1000; }
            if (health <= 0) { ProcessDeath(); }
        }
    }

    public void ModifyMorale(int moraleChange)
    {
        morale = morale + moraleChange;
        if (morale > 1000) { morale = 1000; }
        if (morale < 0) { morale = 0; }
    }

    public void ModifyCorruption(int corruptionChange)
    {
        corruption = corruption + corruptionChange;
        if (corruption > 1000) { processCorruptionDefeat(); }
        if (corruption < 0) { corruption = 0; }
    }




    // Start is called before the first frame update
    void Start()
    {
    anim = GetComponent<Animator>();      
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) && atShrine) 
        {
            if (currentShrine.shrineUsed == false) {
                ModifyMorale(500);
                currentShrine.shrineUsed = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)) { currentHotkey = 1; selectedItemMover.select1stHotkey(); }
        else if (Input.GetKeyDown(KeyCode.Alpha2)) { currentHotkey = 2; selectedItemMover.select2ndHotkey(); }
        else if (Input.GetKeyDown(KeyCode.Alpha3)) { currentHotkey = 3; selectedItemMover.select3rdHotkey(); }
        else if (Input.GetKeyDown(KeyCode.Alpha4)) { currentHotkey = 4; selectedItemMover.select4thHotkey(); }
    }
}
