﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameEndCheck : MonoBehaviour
{
//    public PlayerController playerController;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Krieger")
        {
            GameObject.Find("GameOverScreen").GetComponent<GameOverSceneController>().TriggerGameOverScene();
        }

    }
}
