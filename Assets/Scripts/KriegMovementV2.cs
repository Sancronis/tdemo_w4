﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using Debug = UnityEngine.Debug;


public class KriegMovementV2 : MonoBehaviour
{
    private Rigidbody2D playerRigidbody2D;
    public Transform attackPos;
    public Transform attackPos2;
    public float jumpHeight = 375f;
    public float attackRange = 0.25f;
    public float attackStart = 0.2f;
    public float moveSpeed = 5f;
    public const float speed = 20f;
    private float betweenAttack;
    public LayerMask enemyDetector;
    //    public int health; Handled by PlayerController
    //    public int damage;  Now based on held item
    private bool isJumping = false;
    public float knockback = 5f;
    public float knockbackLength = 0.2f;

    public PlayerController playerController;
    public InventoryManager inventoryManager;
    public ScoreTracker scoreTracker;

    public AudioSource attackMiss;
    public AudioSource jump;
    public AudioSource attackHit;
    public AudioSource potion;
    public AudioSource playerHit;
    public AudioSource enemyHit;


    private void Awake()
    {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        if (playerController.alive)
        {
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
            transform.position += movement * Time.deltaTime * moveSpeed;


            if (Input.GetKeyDown(KeyCode.Space) && !isJumping)
            {
                playerRigidbody2D.AddForce(Vector2.up * jumpHeight);
                jump.Play();
                isJumping = true;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                gameObject.GetComponent<Collider2D>().enabled = false;
                Invoke("Jumper", 0.5f);
            }
            if (betweenAttack <= 0)
            {
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    if (inventoryManager.playerInventory[playerController.currentHotkey - 1] != null) //Is the player actually holding anything?
                    {
                        if (inventoryManager.playerInventory[playerController.currentHotkey - 1].damageInflicted != 0)  //Is the player holding a spade?
                        {
                            Collider2D[] enemiesToHit = Physics2D.OverlapCircleAll(attackPos.position, attackRange, enemyDetector);
                            int preAttackEnemyCount = enemiesToHit.Length;
                            if (preAttackEnemyCount != 0) { attackHit.Play(); enemyHit.Play(); }
                            else { attackMiss.Play(); }
                            for (int i = 0; i < enemiesToHit.Length; i++)
                            {
                                enemiesToHit[i].GetComponent<EnemyDamege>().TakeDamege(inventoryManager.playerInventory[playerController.currentHotkey - 1].damageInflicted); //This happens again if the player is berserk.
                                if (playerController.berserkerTimeLeft != 0) { enemiesToHit[i].GetComponent<EnemyDamege>().TakeDamege(inventoryManager.playerInventory[playerController.currentHotkey - 1].damageInflicted); }
                            }


                            Collider2D[] enemiesToHit2 = Physics2D.OverlapCircleAll(attackPos2.position, attackRange, enemyDetector);
                            preAttackEnemyCount = enemiesToHit2.Length;
                            if (preAttackEnemyCount != 0) { attackHit.Play(); enemyHit.Play(); }
                            else { attackMiss.Play(); }
                            for (int i = 0; i < enemiesToHit2.Length; i++)
                            {
                                enemiesToHit2[i].GetComponent<EnemyDamege>().TakeDamege(inventoryManager.playerInventory[playerController.currentHotkey - 1].damageInflicted); //This happens again if the player is berserk.
                                if (playerController.berserkerTimeLeft != 0) { enemiesToHit2[i].GetComponent<EnemyDamege>().TakeDamege(inventoryManager.playerInventory[playerController.currentHotkey - 1].damageInflicted); }
                            }
                            //added this section so theres now an attack section always infront of him




                            int postAttackEnemyCount = enemiesToHit.Length;
                            scoreTracker.ModifyScore(preAttackEnemyCount - postAttackEnemyCount); //Add 1 score for every dead enemy (could change this if we want).
                            playerController.ModifyMorale(100 * (preAttackEnemyCount - postAttackEnemyCount)); //Could rebalance that.
                        }
                        if (inventoryManager.playerInventory[playerController.currentHotkey - 1].isPassive == false) //Is the player holding something that can be used in their hands?
                        {
                            playerController.ModifyHealth(inventoryManager.playerInventory[playerController.currentHotkey - 1].useHealth);
                            playerController.ModifyMorale(inventoryManager.playerInventory[playerController.currentHotkey - 1].useMorale);
                            playerController.ModifyCorruption(inventoryManager.playerInventory[playerController.currentHotkey - 1].useCorruption);
                            if (inventoryManager.playerInventory[playerController.currentHotkey - 1].berserkerTime != 0)
                            {
                                playerController.berserkerTimeLeft += inventoryManager.playerInventory[playerController.currentHotkey - 1].berserkerTime;
                            }
                            if (inventoryManager.playerInventory[playerController.currentHotkey - 1].limitedCharges == true)
                            {
                                potion.Play();
                                inventoryManager.playerInventory[playerController.currentHotkey - 1].charges--;
                                if (inventoryManager.playerInventory[playerController.currentHotkey - 1].charges == 0)
                                {  //Put charges back to default, remove the item from inventory and take it off the hotbar.
                                    inventoryManager.playerInventory[playerController.currentHotkey - 1].charges = inventoryManager.playerInventory[playerController.currentHotkey - 1].startingCharges;
                                    inventoryManager.playerInventory[playerController.currentHotkey - 1] = null;
                                    inventoryManager.updateIcons();
                                }
                            }
                        }
                    }
                }
                betweenAttack = attackStart;
            }
            else
            {
                betweenAttack -= Time.deltaTime;
            }
        }
        /*        if (health <= 0)  Commented out as death is handled in PlayerController.
                {
                    Destroy(gameObject);

                }
        */
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            isJumping = false;
        }
        if (collision.gameObject.layer == 8)
        {
            //            health -= 5;
            playerController.ModifyHealth(-50); //(playerController's health is from 1-1000
            playerHit.Play();
            Vector3 direction = transform.position - collision.gameObject.transform.position;
            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                if (direction.x > 0)
                {
                    playerRigidbody2D.velocity = new Vector3(knockback, knockback);
                }
                else
                {
                    playerRigidbody2D.velocity = new Vector3(-knockback, knockback);
                }

            }
            else
            {

                if (direction.y > 0)
                {
                    playerRigidbody2D.velocity = new Vector3(0, knockback);
                }
            }
        }
    }
    public void Jumper()
    {
        gameObject.GetComponent<Collider2D>().enabled = true;
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
        Gizmos.DrawWireSphere(attackPos2.position, attackRange);
    }
}
