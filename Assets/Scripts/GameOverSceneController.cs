﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverSceneController : MonoBehaviour
{
    public Transform Camera;
    SpriteRenderer sr;

    Color Grey = new Color(25, 25, 25, 1);
    Color Transparent = new Color(25, 25, 25, 0);

    bool isShown;

    //Lerp Stuff
    bool lerpRunning = false;
    float timeElapsed;
    float startTime;
    float targetTime;
    float lerpDuration = 1f;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        sr.color = Transparent;
        isShown = false;
    }

    void Update()
    {
        transform.position = new Vector3(Camera.position.x, Camera.position.y, -9f);

        if (lerpRunning)
        {
            RunLerp();
        }

        if (Input.anyKeyDown && !lerpRunning && isShown)
        {
            GoToMenu();
            isShown = false;
        }
    }

    public void TriggerGameOverScene() //Call this from another function (GameObject.Find("GameOverScreen").GetComponent<GameOverSceneController>().TriggerGameOverScene();)
    {
        startTime = Time.time;
        targetTime = Time.time + 1f;
        isShown = false;
        lerpRunning = true;
    }

    void RunLerp()
    {
        timeElapsed += Time.deltaTime;

        sr.color = Color.Lerp(Transparent, Grey, timeElapsed / lerpDuration);

        if (timeElapsed >= lerpDuration)
        {
            lerpRunning = false;
            isShown = true;
        }
    }

    void GoToMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
}